const express = require("express");
const bodyParser = require('body-parser');
const TempoApi = require('tempo-client').default;

const moment = require("moment");

const tempo = new TempoApi({
    protocol: 'https',
    host: 'api.tempo.io',
    bearerToken: 'nlvDTKYpBuGVcqONZllKvOZnsvBn3g',
    apiVersion: '3'
});


var cors = require('cors');

var app = express();

app.options('*', cors());

var router = express.Router();
var request = require("request");

var username = 'joan.lloria@cabsa.es';
var password = 'A5ohyDbI46Na9nOAcykBC2F5';
var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

var JIRA_API_V3_URL = 'https://insightview.atlassian.net/rest/api/3';
var JIRA_API_AGILE_V1_URL = 'https://insightview.atlassian.net//rest/agile/1.0';

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', cors(), function (req, res) {
    res.json({message: 'Greetings from API JIRA Dashboard'});
});

// middleware to use for all requests
router.use(function(req, res, next) {
    next(); // make sure we go to the next routes and don't stop here
});

router.get('/boards/search/:startAt', cors(), function (req, res, next) {
    var url = JIRA_API_AGILE_V1_URL + '/board?startAt=' + req.params.startAt;
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.get('/epics/:boardId/:startAt', cors(), function (req, res, next) { 

    var url = JIRA_API_AGILE_V1_URL + '/board/' + req.params.boardId + '/epic?startAt=' + req.params.startAt;
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.get('/projects/:boardId/:startAt', cors(), function (req, res, next) { 

    var url = JIRA_API_AGILE_V1_URL + '/board/' + req.params.boardId + '/project?startAt=' + req.params.startAt;
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.get('/sprints/:boardId/:startAt', cors(), function (req, res, next) { 

    var url = JIRA_API_AGILE_V1_URL + '/board/' + req.params.boardId + '/sprint?startAt=' + req.params.startAt;
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.get('/issues/board/:boardId/:startAt', cors(), function (req, res, next) { 

    var url = JIRA_API_AGILE_V1_URL + '/board/' + req.params.boardId + '/issue?startAt=' + req.params.startAt;
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.get('/issues/epic/:epicId/:startAt', cors(), function (req, res, next) { 

    var url = JIRA_API_AGILE_V1_URL + '/epic/' + req.params.epicId + '/issue?startAt=' + req.params.startAt;
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.get('/issues/project/:projectId/:startAt', cors(), function (req, res, next) { 

    var url = JIRA_API_AGILE_V1_URL + '/project/' + req.params.projectId + '/issue?startAt=' + req.params.startAt;
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.get('/issues/sprint/:sprintId/:startAt', cors(), function (req, res, next) { 

    var url = JIRA_API_AGILE_V1_URL + '/sprint/' + req.params.sprintId + '/issue?startAt=' + req.params.startAt;
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.get('/board/:id/backlog/:startAt', cors(), function (req, res, next) { 

    var url = JIRA_API_AGILE_V1_URL + '/board/' + req.params.id + '/backlog?startAt=' + req.params.startAt;
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.get('/board/:id/version/:startAt', cors(), function (req, res, next) { 

    var url = JIRA_API_AGILE_V1_URL + '/board/' + req.params.id + '/version?startAt=' + req.params.startAt;
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.get('/board/:id', cors(), function (req, res, next) { 

    var url = JIRA_API_AGILE_V1_URL + '/board/' + req.params.id;
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.get('/epic/:id', cors(), function (req, res, next) { 

    var url = JIRA_API_AGILE_V1_URL + '/epic/' + req.params.id;
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.get('/sprint/:id', cors(), function (req, res, next) { 

    var url = JIRA_API_AGILE_V1_URL + '/sprint/' + req.params.id;
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.get('/issue/:id', cors(), function (req, res, next) { 

    var url = JIRA_API_AGILE_V1_URL + '/issue/' + req.params.id;
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.get('/issues/search/:from/:to/:startAt', cors(), function (req, res, next) { 

    var url = JIRA_API_V3_URL + '/search?maxResults=1000&startAt=' + req.params.startAt + '&jql=worklogDate >= "' + req.params.from + '" AND worklogDate <= "' + req.params.to + '" ORDER BY created DESC';
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.get('/tempo/accounts', cors(), function (req, res, next) {

    tempo.accounts.get()
        .then(accounts => {
           res.send(accounts);
        })
        .catch(err => {
            console.log("error");
            console.log(err);
        });
});

router.get('/tempo/worklogs/:worklogId', cors(), function (req, res, next) {

    tempo.worklogs.getWorklog(req.params.worklogId)
        .then(worklogs => {
            res.send(worklogs);
        })
        .catch(err => {
            console.log("error");
            console.log(err);
        });
});

router.get('/tempo/worklogs/:userAccountId/:from/:to/:startAt', cors(), function (req, res, next) {

    const options = {
        from: req.params.from,
        to: req.params.to,
        offset: req.params.startAt,
        limit: 500
    };

    console.log(req.params.userAccountId, options);

    tempo.worklogs.getForUser(req.params.userAccountId, options)
        .then(worklogData => {
            res.send(worklogData);
        })
        .catch(err => {
            console.log("error");
            console.log(err);
        });
});

router.get('/user/account/:userAccountId', cors(), function (req, res, next) {
    var url = JIRA_API_V3_URL + '/user?accountId=' + req.params.userAccountId;
    console.log(url);

    request(
        {
            url : url,
            headers : {
                "Authorization" : auth
            }
        },
        function (error, response, body) {
            if(error) {
                console.log('Error calling Atlassian API');
                res.json(error);
            }
        }
    ).pipe(res);
});

router.post('/querybuilder', cors(), function(req, res) {

    switch (req.body.subject) {
        case "worklogs":
            var dateFrom = res.dateFrom = moment(req.body.dateFrom, "DD-MM-YYYY").format("YYYY-M-D");
            var dateTo = res.dateFrom = moment(req.body.dateTo, "DD-MM-YYYY").format("YYYY-M-D");
            
            const options = {
                from: dateFrom,
                to: dateTo,
                offset: "0",
                limit: 500
            };

            tempo.worklogs.getForUser(req.body.userId, options)
                .then(worklogData => {
                    res.send(worklogData);
                })
                .catch(err => {
                    res.send(err);
                });
            
            break;

        case "sprint":

            var url = JIRA_API_V3_URL + '/user?accountId=' + req.body.userId;

            request(
                {
                    url : url,
                    headers : {
                        "Authorization" : auth
                    }
                },
                function (error, response, body) {
                    if(error) {
                        console.log('Error calling Atlassian API');
                        res.json(error);
                    }
                }
            ).pipe(res);

            break;
    }

    
});

app.use('/api', router);
app.use(cors())

app.listen(3000, () => {
    console.log('Listening on port 3000');
});





